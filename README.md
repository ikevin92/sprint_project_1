# Backend "Delilah Restó"

Sprint Project #1 del curso de Desarrollo Web Back End de Acámica.


## Recursos y tecnologías utilizadas 🛠️

- Node.js
- Nodemon
- Morgan
- Express
- Swagger para documentación de API

## Pre-requisitos 📋

Tener instaldo Node.js

## 1 - Clonar proyecto 🚀

Iniciar una terminal en Visual Studio Code y clonar el repositorio.

`git clone https://gitlab.com/ikevin92/sprint_project_1.git.`

## 2 - Instalación de dependencias 🔧

Una vez clonado el repositorio , dirigirse por medio de la consola a la carpeta raiz del proyecto 
y escribir el siguiente comando :

```
npm install
```

## 3 - Iniciando el servidor ⚙️

Iniciar el servidor por consola usando el siguiente comando :

```
 npm start
```

## 4 - Listo para usar! 🔩

Testear los endpoints provistos desde Swagger .

[Ingresar a Swagger API DOCS](http://localhost:4000/api-docs/)

### Administrador : 

User : admin

Password : admin123

### Usuario Prueba : 

User : user

Password : user123
