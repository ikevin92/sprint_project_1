const statusOrderArray = [
  { id: '1', name: 'pendiente' },
  { id: '2', name: 'confirmado' },
  { id: '3', name: 'en_preparacion' },
  { id: '4', name: 'enviado' },
  { id: '5', name: 'entregado' },
];

module.exports = statusOrderArray;
