const { v4: uudiv4 } = require('uuid');

const paymentMethodsArray = [
  {
    // id: uudiv4(),
    id: '1',
    name: 'Checks',
  },
  {
    // id: uudiv4(),
    id: '2',
    name: 'Cash',
  },
  {
    // id: uudiv4(),
    id: '3',
    name: 'Credit cards',
  },
];

// Cash.
//     Checks.;
// Debit cards.;
// Credit cards.;
// Mobile payments.;
// Electronic bank transfers.

module.exports = paymentMethodsArray;
