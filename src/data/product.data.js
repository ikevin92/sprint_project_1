const { v4: uudiv4 } = require('uuid');

const productsArray = [
  {
    // id: uudiv4(),
    id: '1',
    name: 'Sandwich Queso',
    price: 310,
  },
  {
    // id: uudiv4(),
    id: '2',
    name: 'Hamburguesa Clasica',
    price: 350,
  },
  {
    // id: uudiv4(),
    id: '3',
    name: 'Focaccia',
    price: 189,
  },
  {
    // id: uudiv4(),
    id: '4',
    name: 'Sandwich Vegetariano',
    price: 140,
  },
  {
    // id: uudiv4(),
    id: '5',
    name: 'HamSpecial',
    price: 120,
  },
];

module.exports = productsArray;
