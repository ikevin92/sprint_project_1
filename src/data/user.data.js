const { v4: uudiv4 } = require('uuid');

const usersArray = [
  {
    // id: uudiv4(),
    id: '1',
    username: 'admin',
    email: 'admin@email.com',
    name: 'administrador',
    lastname: 'superusuario',
    password: 'admin123',
    isAdmin: true,
  },
  {
    // id: uudiv4(),
    id: '2',
    username: 'user',
    email: 'user@email.com',
    name: 'user',
    lastname: 'system',
    password: 'user123',
    isAdmin: false,
  },
  {
    // id: uudiv4(),
    id: '3',
    username: 'user2',
    email: 'user2@email.com',
    name: 'user2',
    lastname: 'system2',
    password: 'user123',
    isAdmin: false,
  },
];

module.exports = usersArray;
