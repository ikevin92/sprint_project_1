const { v4: uudiv4 } = require('uuid');

const ordersArray = [
  {
    id: '1',
    items: [
      {
        product: '1',
        amount: 10,
        subtotal: 1000,
      },
      {
        product: '2',
        amount: 3,
        subtotal: 130,
      },
    ],
    address: 'CALLE 19 # 5L-19',
    status: 'pendiente',
    paymentMethod: '1',
    user: '1',
  },
  {
    id: '2',
    items: [
      {
        product: '4',
        amount: 3,
        subtotal: 200,
      },
      {
        product: '1',
        amount: 6,
        subtotal: 120,
      },
    ],
    address: 'CARRERA 19 #5L-19',
    paymentMethod: '2',
    status: 'confirmado',
    user: '2',
  },
  {
    id: '3',
    items: [
      {
        product: '2',
        amount: 3,
        subtotal: 22,
      },
      {
        product: '1',
        amount: 6,
        subtotal: 123,
      },
    ],
    address: 'CARRERA 19 #5L-19',
    paymentMethod: '2',
    status: 'confirmado',
    user: '3',
  },
];

module.exports = ordersArray;
