const { v4: uudiv4 } = require('uuid');
const usersArray = require('../data/user.data');

class User {
  id = '';
  email = '';
  username = '';
  name = '';
  lastname = '';
  password = '';
  isAdmin = false;

  constructor(username, email, name, lastname, password, id = uudiv4()) {
    this.isAdmin = false;
    this.username = username;
    this.email = email;
    this.name = name;
    this.lastname = lastname;
    this.password = password;
    this.id = id;
  }

  addUser() {
    console.log('this', this);
    console.log('addUser');
    return usersArray.push(this);
  }

  findAll() {
    return usersArray;
  }
}

module.exports = User;
