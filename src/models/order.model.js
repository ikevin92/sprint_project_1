const User = require('./user.model');
const { v4: uudiv4 } = require('uuid');
const ordersArray = require('../data/orders.data');
const PaymentMethod = require('./paymentMethod.model');

class Order {
  id = '';
  items = [];
  status = '1';
  address = '';
  user = '';
  paymentMethod = '';

  constructor(
    items = [],
    address = '',
    user = '',
    paymentMethod = '',
    status = '1',
    id = uudiv4(),
  ) {
    this.items = items;
    this.address = address;
    this.status = status;
    this.id = id;
    this.user = user;
    this.paymentMethod = paymentMethod;
  }

  addOrder() {
    // console.log( 'this', this );
    ordersArray.push(this);
  }

  findAll() {
    return ordersArray;
  }

  findOne(id) {
    // return ordersArray.findIndex( o => o.id === id );
    return ordersArray.filter((o) => o.id === id);
  }

  updateById(index, data) {
    console.log({ data: data });
    return (ordersArray[index] = data);
  }

  deleteById(index) {
    return ordersArray.splice(index, 1);
  }
}

module.exports = Order;
