const { v4: uudiv4 } = require('uuid');
const paymentMethodsArray = require('../data/paymentM.data');

class PaymentMethod {
  id = '';
  name = '';

  constructor(name = '', id = uudiv4()) {
    this.id = id;
    this.name = name;
  }

  addPaymentMethod() {
    // console.log( 'this', this );
    return paymentMethodsArray.push(this);
  }

  findAll() {
    return paymentMethodsArray;
  }

  findOne(id) {
    return paymentMethodsArray.findIndex((pm) => pm.id === id);
  }

  updateById(index) {
    return (paymentMethodsArray[index] = this);
  }

  deleteById(index) {
    return paymentMethodsArray.splice(index, 1);
  }
}
module.exports = PaymentMethod;
