const { v4: uudiv4 } = require('uuid');
const productsArray = require('../data/product.data');

class Product {
  id = '';
  name = '';
  price = 0;

  constructor(name = '', price = '', id = uudiv4()) {
    this.id = id;
    this.name = name;
    this.price = parseInt(price);
  }

  addProduct() {
    // console.log( 'this', this );
    productsArray.push(this);
  }

  findAll() {
    return productsArray;
  }

  findOne(id) {
    return productsArray.findIndex((product) => product.id === id);
  }

  updateById(index) {
    return (productsArray[index] = this);
  }

  deleteById(index) {
    return productsArray.splice(index, 1);
  }
}

module.exports = Product;
