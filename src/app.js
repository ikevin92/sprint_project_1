const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const docs = require('./docs');
const swaggerOptions = require('./swagger/swaggerOptions');

//importacion de rutas
const userRoutes = require('./routes/user.routes');
const productsRoutes = require('./routes/product.routes');
const paymentMethodRoutes = require('./routes/paymentMethods.routes');
const orderRoutes = require('./routes/order.routes');

//middelwares
app.use(cors());
app.use(morgan('dev'));
app.use(express.json()); // para que entienda el formato json

//routes
app.use('/api/products', productsRoutes);
// app.use( '/api/auth', authRoutes );
app.use('/api/users', userRoutes);
app.use('/api/payment_methods', paymentMethodRoutes);
app.use('/api/order', orderRoutes);

// Documentacion swagger
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(docs));

//exportamos
module.exports = app;
