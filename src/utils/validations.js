const usersArray = require('../data/user.data');
const { Base64 } = require('js-base64');
const User = require('../models/user.model');

const emailRegex =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

const validationRegistro = (User) => {
  console.log('entro a validar registro');
  const { email, name, lastname, password, username } = User;
  console.log({ password });

  // validacion de campos vacios
  // const res = validationFields( User );
  if (!validationFields(User).state) return validationFields(User);

  //valicacion exista password
  if (!password) {
    return {
      state: false,
      msg: 'password was not entered',
    };
  }

  if (!emailRegex.test(email)) {
    console.log('email invalido');
    return {
      state: false,
      msg: 'invalid email',
    };
  }

  if (password.length < 6) {
    return {
      state: false,
      msg: 'password less than 6 characters',
    };
  }

  //mails duplicados
  const existEmail = User.findAll().filter(
    (user) => user.email === email || user.username == username,
  );

  console.log({ existEmail });

  if (existEmail.length > 0) {
    return {
      state: false,
      msg: 'Email and Username is registered with another user',
    };
  }

  return { state: true };
};

const validationLogin = (username, password) => {
  if (username.trim === '' || password === '') {
    return {
      state: false,
      msg: 'All fields are required',
    };
  }

  const searchUserPassword = usersArray.filter(
    (user) => user.username === username && user.password === password,
  );

  if (searchUserPassword.length < 1) {
    return {
      state: false,
      msg: 'wrong credentials',
    };
  }

  return {
    state: true,
  };
};

const decodeUser = (auth) => {
  const userFind = new User();
  const [basic, token] = auth.split(' ');
  const [user, password] = Base64.decode(token).split(':');

  const userFilter = userFind.findAll().filter((u) => u.username === user);

  const id = userFilter.length > 0 ? userFilter[0].id : null;

  return { username: user, id };
};

const validationFields = (Objeto) => {
  for (const prop in Objeto) {
    console.log(`${prop}: ${Objeto[prop]}`);

    if (Objeto[prop].toString().trim() === '') {
      return {
        state: false,
        msg: 'All fields are required',
      };
    }

    return {
      state: true,
    };
  }
};

const validationProduct = (Product) => {
  const { name, price } = Product;

  if (!validationFields(Product).state) return validationFields(Product);

  if (isNaN(price)) {
    return {
      state: false,
      msg: 'price must be numerical value',
    };
  }

  return { state: true };
};

const validationPaymentMethod = (PaymentMethod) => {
  console.log('validacion');

  if (!validationFields(PaymentMethod).state)
    return validationFields(PaymentMethod);

  return { state: true };
};

const validationOrder = (Order) => {
  console.log('validation order');

  console.log({ orden_validation: Order.items });

  if (!validationFields(Order).state) return validationFields(Order);

  if (Order.items.length < 1) {
    return {
      state: false,
      msg: 'the order must have at least one product',
    };
  }

  // validando duplicados
  const busqueda = Order.items.reduce((acc, orderItem) => {
    acc[orderItem.product] = ++acc[orderItem.product] || 0;
    return acc;
  }, {});

  const duplicados = Order.items.filter((item) => {
    return busqueda[item.product];
  });

  if (duplicados.length > 0) {
    return {
      state: false,
      msg: 'duplicate items are found in the order',
    };
  }

  return { state: true };
};

const validationAdmin = (username) => {
  const user = new User();

  const userFinded = user
    .findAll()
    .filter((user) => user.username === username);

  if (!userFinded[0].isAdmin)
    return { status: false, userId: userFinded[0].id };

  return {
    status: true,
    userId: userFinded[0].id,
  };
};

module.exports = {
  validationRegistro,
  validationLogin,
  validationPaymentMethod,
  decodeUser,
  validationProduct,
  validationOrder,
  validationAdmin,
};
