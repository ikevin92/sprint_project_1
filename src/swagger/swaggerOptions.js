const swaggerOptions = {
  definition: {
    openapi: "3.0.3",
    info: {
      title: "Sprint1 edit API",
      version: "1.0.0",
      description: "Documentation"
    },
    servers: [
      {
        url: 'http://localhost:4000',
        description: "Local server"
      },
    ],
    components: {
      securitySchemes: {
        basicAuth: {
          type: "http",
          scheme: "basic"
        }
      }
    },
    security: [
      {
        basicAuth: []
      }
    ]
  },
  apis: [ "./routes/*.js" ]
};

module.exports = swaggerOptions;