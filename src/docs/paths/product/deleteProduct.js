module.exports = {
  delete: {
    tags: ['Product'],
    description: 'delete products',
    operationId: 'deleteProducts',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'product successfully deleted',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'product deleted successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
