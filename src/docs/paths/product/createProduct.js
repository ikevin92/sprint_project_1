module.exports = {
  post: {
    tags: ['Product'],
    description: 'add product',
    operationId: 'addProduct',
    parameters: [],
    security: [
      {
        basicAuth: [],
      },
    ],
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/ProductInput',
          },
        },
      },
    },
    responses: {
      201: {
        description: 'Product created successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
