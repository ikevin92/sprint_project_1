module.exports = {
  get: {
    tags: ['Product'],
    description: 'get products',
    operationId: 'getProducts',
    parameters: [],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'Products were obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Product',
            },
          },
        },
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
