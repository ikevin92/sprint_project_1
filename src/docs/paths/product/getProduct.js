module.exports = {
  get: {
    tags: ['Product'],
    description: 'Get product',
    operationId: 'getProduct',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'Id of product',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'Product is obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Product',
            },
          },
        },
      },
      401: {
        description: 'Unauthorized',
      },
      404: {
        description: 'Product is not found',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error',
              example: {
                message: "We can't find the product",
                internal_code: 'Invalid id',
              },
            },
          },
        },
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
