module.exports = {
  put: {
    tags: ['Product'],
    description: 'update product',
    operationId: 'updateProduct',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'Id of product to be updated',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/ProductInput',
          },
        },
      },
    },
    responses: {
      201: {
        description: 'Product updated successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
