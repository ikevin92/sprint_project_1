const createProduct = require('./createProduct');
const getProducts = require( './getProducts' );
const getProduct = require( './getProduct' );
const updateProduct = require('./updateProduct');
const deleteProduct = require( './deleteProduct' );

module.exports = {
  '/products': {
    ...createProduct,
    ...getProducts
	},
  '/products/{id}': {
    ...updateProduct,
    ...getProduct,
    ...deleteProduct
	},
};
