module.exports = {
  get: {
    tags: ['PaymentMethod'],
    description: 'get PaymentMethods',
    operationId: 'getPaymentMethods',
    parameters: [],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'PaymentMethods were obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/PaymentMethod',
            },
          },
        },
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
