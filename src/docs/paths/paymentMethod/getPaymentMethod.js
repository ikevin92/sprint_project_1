module.exports = {
  get: {
    tags: ['PaymentMethod'],
    description: 'Get paymentMethod',
    operationId: 'getPaymentMethod',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'Id of payment Method',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'Payment Method is obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/PaymentMethod',
            },
          },
        },
      },
      401: {
        description: 'Unauthorized',
      },
      404: {
        description: 'Payment Method is not found',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error',
              example: {
                message: "We can't find the paymentMethod",
                internal_code: 'Invalid id',
              },
            },
          },
        },
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
