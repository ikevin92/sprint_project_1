module.exports = {
  delete: {
    tags: ['PaymentMethod'],
    description: 'delete PaymentMethod',
    operationId: 'deletePaymentMethod',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'PaymentMethod successfully deleted',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'PaymentMethod deleted successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
