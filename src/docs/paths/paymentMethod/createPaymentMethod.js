module.exports = {
  post: {
    tags: ['PaymentMethod'],
    description: 'add PaymentMethod',
    operationId: 'addPaymentMethod',
    parameters: [],
    security: [
      {
        basicAuth: [],
      },
    ],
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/PaymentMethodInput',
          },
        },
      },
    },
    responses: {
      201: {
        description: 'PaymentMethod created successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
