const createPaymentMethod = require('./createPaymentMethod');
const getPaymentMethods = require('./getPaymentMethods');
const getPaymentMethod = require('./getPaymentMethod');
const updatePaymentMethod = require('./updatePaymentMethod');
const deletePaymentMethod = require('./deletePaymentMethod');

module.exports = {
  '/payment_methods': {
    ...createPaymentMethod,
    ...getPaymentMethods,
  },
  '/payment_methods/{id}': {
    ...updatePaymentMethod,
    ...getPaymentMethod,
    ...deletePaymentMethod,
  },
};
