module.exports = {
  put: {
    tags: ['PaymentMethod'],
    description: 'update PaymentMethod',
    operationId: 'updatePaymentMethod',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'Id of PaymentMethod to be updated',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/PaymentMethodInput',
          },
        },
      },
    },
    responses: {
      201: {
        description: 'PaymentMethod updated successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
