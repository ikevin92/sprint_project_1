const createOrder = require('./createOrder');
const getOrders = require('./getOrders');
const getOrder = require('./getOrder');
const updateOrder = require('./updateOrder');
const changeStatusOrder = require('./changeStatusOrder');
const deleteOrder = require('./deleteOrder');

module.exports = {
  '/order': {
    ...createOrder,
    ...getOrders,
  },
  '/order/{id}': {
    ...updateOrder,
    ...getOrder,
    ...deleteOrder,
  },
  '/order/change-status/{id}': {
    ...changeStatusOrder,
  },
};
