module.exports = {
  put: {
    tags: ['Orders'],
    description: 'change Status Order',
    operationId: 'change Status Order',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'Id of Order to be updated',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/StatusOrderInput',
          },
        },
      },
    },
    responses: {
      201: {
        description: 'Order status change successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
