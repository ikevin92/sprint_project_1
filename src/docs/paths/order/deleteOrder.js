module.exports = {
  delete: {
    tags: ['Orders'],
    description: 'delete Order',
    operationId: 'deleteOrder',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'Order successfully deleted',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'Order deleted successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
