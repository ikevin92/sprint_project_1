module.exports = {
  post: {
    tags: ['Orders'],
    description: 'add Order',
    operationId: 'addOrder',
    parameters: [],
    security: [
      {
        basicAuth: [],
      },
    ],
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/OrderInput',
          },
        },
      },
    },
    responses: {
      200: {
        description: 'Order created successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
