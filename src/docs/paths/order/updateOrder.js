module.exports = {
  put: {
    tags: ['Orders'],
    description: 'update Order',
    operationId: 'updateOrder',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'Id of Order to be updated',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/OrderInput',
          },
        },
      },
    },
    responses: {
      201: {
        description: 'Order updated successfully',
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
