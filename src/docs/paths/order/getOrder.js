module.exports = {
  get: {
    tags: ['Orders'],
    description: 'Get Order, this ',
    operationId: 'getOrder',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/id',
        },
        required: true,
        description: 'Id of order',
      },
    ],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'Order is obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Order',
            },
          },
        },
      },
      401: {
        description: 'Unauthorized',
      },
      404: {
        description: 'Order is not found',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error',
              example: {
                message: "We can't find the Order",
                internal_code: 'Invalid id',
              },
            },
          },
        },
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
