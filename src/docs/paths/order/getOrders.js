module.exports = {
  get: {
    tags: ['Orders'],
    description:
      'this endpoint validates if it is the user with administrator privileges it shows all the orders. For users without privileges it only shows those assigned to it',
    operationId: 'getOrders',
    parameters: [],
    security: [
      {
        basicAuth: [],
      },
    ],
    responses: {
      200: {
        description: 'Orders were obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Order',
            },
          },
        },
      },
      401: {
        description: 'Unauthorized',
      },
      500: {
        description: 'Server error',
      },
    },
  },
};
