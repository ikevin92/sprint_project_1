const createUser = require( './createUser' );
const createProduct = require( './createUser' );
const login = require( './login' );

module.exports = {
  '/users/register': {
    ...createUser
  },
  '/users/login': {
    ...login
  },
};