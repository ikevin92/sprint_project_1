module.exports = {
  post: {
    summary: 'Login',
    operationId: 'Login User',
    description: 'Login action',
    responses: {
      201: {
        description: 'Login successfully',
      },
      400: {
        description: 'invalid input, object invalid',
      },
      409: {
        description: 'Login Failed',
      },
    },
    requestBody: {
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/Login',
          },
        },
      },
      description: 'Login User',
    },
    tags: ['User'],
  },
};
