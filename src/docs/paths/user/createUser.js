module.exports = {
  post: {
    summary: 'adds an user',
    operationId: 'addUsers',
    description: 'Adds an item to the system',
    responses: {
      201: {
        description: 'User Registered successfully',
      },
      400: {
        description: 'invalid input, object invalid',
      },
      409: {
        description: 'User already exists',
      },
    },
    requestBody: {
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/User',
          },
        },
      },
      description: 'User add',
    },
    tags: ['User'],
  },
};
