const todos = require('./todos');
const product = require('./product');
const user = require('./user');
const order = require('./order');
const paymentMethod = require('./paymentMethod');

module.exports = {
  paths: {
    // ...todos,
    ...user,
    ...product,
    ...paymentMethod,
    ...order
  }
};
