module.exports = {
  components: {
    schemas: {
      id: {
        type: 'string',
        description: 'An id of a item',
        example: '1',
      },
      ProductInput: {
        type: 'object',
        required: ['name', 'price'],
        properties: {
          name: {
            type: 'string',
            description: 'product name',
            example: 'Hamburger',
          },
          price: {
            type: 'number',
            description: 'price product',
            example: 100,
          },
        },
      },
      Product: {
        type: 'object',
        properties: {
          id: {
            type: 'string',
            description: 'Product identification id',
            example: '1',
          },
          name: {
            type: 'string',
            description: 'product name',
            example: 'Hamburger',
          },
          price: {
            type: 'number',
            description: 'price product',
            example: 100,
          },
        },
      },
      PaymentMethod: {
        type: 'object',
        properties: {
          id: {
            type: 'string',
            description: 'payment method identification id',
            example: '1',
          },
          name: {
            type: 'string',
            description: 'payment method name',
            example: 'Debit Card',
          },
        },
      },
      PaymentMethodInput: {
        type: 'object',
        required: ['name'],
        properties: {
          name: {
            type: 'string',
            description: 'payment method name',
            example: 'Debit Card',
          },
        },
      },
      ProductOrderInput: {
        type: 'object',
        required: ['product', 'amount', 'subtotal'],
        properties: {
          product: {
            type: 'string',
            description: 'id product',
            example: '1',
          },
          amount: {
            type: 'number',
            description: 'amount product order',
            example: 4,
          },
          subtotal: {
            type: 'number',
            description: 'product subtotal order',
            example: 120,
          },
        },
      },
      Order: {
        type: 'object',
        properties: {
          id: {
            type: 'string',
            description: 'order identification id',
            example: '1',
          },
          items: {
            type: 'array',
            description: 'List Products',
            items: {
              $ref: '#/components/schemas/ProductOrderInput',
            },
          },
          address: {
            type: 'string',
            description: 'address order',
            example: 'CALLE 19 # 5L-19',
          },
          paymentMethod: {
            type: 'string',
            description: 'paymentMethod id',
            example: '1',
          },
          status: {
            $ref: '#/components/schemas/StatusOrder',
          },
          user: {
            type: 'string',
            description: 'user id',
            example: '1',
          },
        },
      },
      OrderInput: {
        type: 'object',
        required: ['items', 'address', 'status', 'user'],
        properties: {
          items: {
            type: 'array',
            description: 'payment method name',
            // example: 'Debit Card',
            items: {
              $ref: '#/components/schemas/ProductOrderInput',
            },
          },
          address: {
            type: 'string',
            description: 'address order',
            example: 'CALLE 19 # 5L-19',
          },
          paymentMethod: {
            type: 'string',
            description: 'paymentMethod id',
            example: '1',
          },
          status: {
            $ref: '#/components/schemas/StatusOrder',
          },
          user: {
            type: 'string',
            description: 'user id',
            example: '1',
          },
        },
      },
      StatusOrder: {
        type: 'string',
        enum: [
          'pendiente',
          'confirmado',
          'en_preparacion',
          'enviado',
          'entregado',
        ],
      },
      StatusOrderInput: {
        type: 'object',
        required: ['status'],
        properties: {
          status: {
            type: 'string',
            description: 'status name',
            example: 'confirmado',
          },
        },
      },
      User: {
        type: 'object',
        required: ['name', 'lastname', 'username', 'email', 'password'],
        properties: {
          name: {
            type: 'string',
            example: 'Kevin',
          },
          lastname: {
            type: 'string',
            example: 'Echeverri',
          },
          username: {
            type: 'string',
            example: 'koel1992',
          },
          email: {
            type: 'string',
            example: 'kevin@email.com',
          },
          password: {
            type: 'string',
            example: '123456',
          },
        },
      },
      Login: {
        type: 'object',
        required: ['username', 'password'],
        properties: {
          username: {
            type: 'string',
            example: 'koel1992',
          },
          password: {
            type: 'string',
            example: '123456',
          },
        },
      },
      Error: {
        type: 'object',
        properties: {
          message: {
            type: 'string',
          },
          internal_code: {
            type: 'string',
          },
        },
      },
    },
    securitySchemes: {
      basicAuth: {
        type: 'http',
        scheme: 'basic',
      },
    },
  },
};
