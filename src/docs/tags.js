module.exports = {
  tags: [
    { name: 'User', description: 'user' },
    { name: 'Product', description: 'products' },
    { name: 'PaymentMethod', description: 'payment Methods' },
    { name: 'Orders', description: 'orders' },
    { name: 'Todo CRUD operations', description: 'tods' },
  ],
};
