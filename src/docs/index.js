const basicInfo = require('./basicInfo');
const servers = require('./servers');
const components = require('./components');
const tags = require('./tags');
const { security } = require('./security');
const paths = require('./paths');

console.log(paths);

module.exports = {
  ...basicInfo,
  ...servers,
  ...components,
  ...tags,
  // ...todos,
  ...paths,
  ...security,
};
