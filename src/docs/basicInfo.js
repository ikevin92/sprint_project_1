module.exports = {
  openapi: '3.0.0',
  info: {
    version: '1.0.0',
    title: 'Sprint1 API',
    description: 'Sprint1 API ACAMICA DWB',
    contact: {
      name: 'Kevin Echeverri',
      email: 'ikevin1992@gmail.com',
      url: 'https://gitlab.com/ikevin92/spring_project_1',
    },
  },
};
