const { Base64 } = require('js-base64');
const Product = require('../models/product.model');

const { decodeUser, validationProduct } = require('../utils/validations');

const getProducts = (req, res) => {
  try {
    const product = new Product();

    const listProducts = product.findAll();

    if (listProducts.length > 0) return res.json(listProducts);

    return res.status(404).json({
      status: false,
      msg: 'no products were found in the database',
    });
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

const getProduct = (req, res) => {
  try {
    const { id } = req.params;

    const product = new Product();

    const findProduct = product.findAll().filter((p) => p.id === id);

    if (findProduct.length > 0) return res.json(findProduct);

    return res.status(404).json({
      status: false,
      msg: 'no products were found in the database',
    });
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

const addProduct = (req, res) => {
  try {
    const { name, price } = req.body;

    const newProduct = new Product(name, price);

    console.log(newProduct);

    // validacion de datos
    const resValidation = validationProduct(newProduct);

    console.log({ resValidation });

    if (resValidation.state) {
      newProduct.addProduct();
      return res.json({
        msg: 'Product created successfully',
        newProduct,
      });
    } else {
      return res.status(500).json(resValidation);
    }
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

const editProduct = (req, res) => {
  try {
    const { name, price } = req.body;

    const { id } = req.params;
    // console.log( { id } );

    const product = new Product(name, price, id);

    // console.log( { product } );

    const indexProduct = product.findAll().findIndex((p) => p.id === id);

    // console.log( { indexProduct } );

    if (indexProduct < 0)
      return res
        .status(404)
        .json({ status: false, msg: 'there is no product with that id' });

    console.log(req.body);

    product.updateById(indexProduct);

    res.json({
      status: true,
      msg: 'product successfully updated',
    });
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

const deleteProduct = (req, res) => {
  try {
    const { id } = req.params;
    // console.log( { id } );

    const product = new Product();

    // console.log( { product } );

    const indexProduct = product.findAll().findIndex((p) => p.id === id);

    // console.log( { indexProduct } );

    if (indexProduct < 0)
      return res
        .status(404)
        .json({ status: false, msg: 'there is no product with that idc' });

    product.deleteById(indexProduct);

    res.json({
      status: true,
      msg: 'product successfully deleted',
    });
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

module.exports = {
  addProduct,
  getProducts,
  editProduct,
  deleteProduct,
  getProduct,
};
