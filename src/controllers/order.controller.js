const Order = require('../models/order.model');
const User = require('../models/user.model');
const {
  decodeUser,
  validationOrder,
  validationAdmin,
} = require('../utils/validations');

const getOrders = (req, res) => {
  try {
    const order = new Order();

    const { authorization } = req.headers;
    console.log({ authorization });
    const { username } = decodeUser(authorization);

    let listOrders = [];

    // validacion de rol para filtrar ordenes
    if (validationAdmin(username).status) {
      listOrders = order.findAll();
    } else {
      listOrders = order
        .findAll()
        .filter((o) => o.user === validationAdmin(username).userId);
    }

    if (listOrders.length > 0) return res.json(listOrders);

    return res.json({
      status: false,
      msg: 'no orders were found in the database',
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};

const getOrder = (req, res) => {
  try {
    console.log(req.headers);
    //obtenemos el usuario
    const { authorization } = req.headers;
    const { id: id_user, username } = decodeUser(authorization);
    console.log({ id_user, username });

    const { id } = req.params;

    const order = new Order();

    const findOrder = order.findAll().filter((o) => o.id === id);
    console.log({ findOrder });

    if (findOrder.length > 0) {
      if (findOrder[0].user === id_user || validationAdmin(username).status) {
        return res.json(findOrder);
      }
    }

    return res.json({
      status: false,
      msg: 'no order were found in the database',
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};

const addOrder = (req, res) => {
  try {
    console.log(req.headers);
    //obtenemos el usuario
    const { authorization } = req.headers;
    const id_user = decodeUser(authorization).id;
    console.log({ id_user });

    console.log('paso el usuario');

    const { items, address, paymentMethod } = req.body;

    console.log(req.body);

    const newOrder = new Order(items, address, id_user, paymentMethod);

    // validacion de datos
    const resValidation = validationOrder(newOrder);

    console.log({ resValidation });

    if (resValidation.state) {
      newOrder.addOrder();
      return res.json({
        msg: 'Successful created order',
        data: newOrder,
      });
    } else {
      return res.status(500).json(resValidation);
    }
  } catch (error) {
    console.log(error);
    console.log('hubo un error');
    return res.status(500).send(error);
  }
};

const editOrder = (req, res) => {
  try {
    //obtener el usuario
    const { authorization } = req.headers;
    const id_user = decodeUser(authorization).id;
    console.log({ id_user });

    const { id } = req.params;

    const order = new Order();

    const indexOrder = order.findAll().findIndex((o) => o.id === id);

    if (indexOrder < 0)
      return res
        .status(404)
        .json({ status: false, msg: 'there is no order with that id' });

    const { items, address, paymentMethod } = req.body;

    const [findOrder] = order.findOne(id);

    console.log({ orden_ant: findOrder });

    // se le asigna los valores al objeto
    findOrder.items = items;
    findOrder.address = address;
    findOrder.paymentMethod = paymentMethod;

    console.log({ orden_desp: findOrder });

    // validacion de datos
    const resValidation = validationOrder(findOrder);

    if (resValidation.state) {
      order.updateById(indexOrder, findOrder);
      res.json({
        status: true,
        msg: 'order successfully updated',
        data: findOrder,
      });
    } else {
      return res.status(500).json(resValidation);
    }
  } catch (error) {
    console.log(error);
    console.log('hubo un error');
    return res.status(500).send(error);
  }
};

const deleteOrder = (req, res) => {
  res.json('deleteOrder');
};

const changeStatusOrder = (req, res) => {
  try {
    const { id } = req.params;
    const order = new Order();

    const indexOrder = order.findAll().findIndex((o) => o.id === id);
    if (indexOrder < 0)
      return res
        .status(404)
        .json({ status: false, msg: 'there is no order with that id' });

    const { status } = req.body;

    const [findOrder] = order.findOne(id);
    console.log({ orden_a_cambiar: findOrder });

    findOrder.status = status;

    order.updateById(indexOrder, findOrder);

    res.json({
      status: true,
      msg: 'order successfully updated',
      data: findOrder,
    });
  } catch (error) {
    console.log(error);
    console.log('hubo un error');
    return res.status(500).send(error);
  }
};

module.exports = {
  getOrders,
  getOrder,
  addOrder,
  editOrder,
  deleteOrder,
  changeStatusOrder,
};
