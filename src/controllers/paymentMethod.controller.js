const PaymentMethod = require('../models/paymentMethod.model');
const { validationPaymentMethod } = require('../utils/validations');

const getPaymentMethods = (req, res) => {
  try {
    const paymentMethod = new PaymentMethod();

    const listPM = paymentMethod.findAll();

    if (listPM.length > 0) {
      return res.json(listPM);
    }

    return res.json({
      status: false,
      msg: 'no products were found in the database',
    });
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

const getPaymentMethod = (req, res) => {
  try {
    const { id } = req.params;

    const paymentMethod = new PaymentMethod();

    const findPaymentMethod = paymentMethod
      .findAll()
      .filter((p) => p.id === id);

    if (findPaymentMethod.length > 0) {
      return res.json(findPaymentMethod);
    }

    return res.json({
      status: false,
      msg: 'no Payment Method were found in the database',
    });
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

const addPaymentMethod = (req, res) => {
  try {
    const { name } = req.body;

    const newPaymentMethod = new PaymentMethod(name);

    console.log(newPaymentMethod);

    // validacion de datos
    const resValidation = validationPaymentMethod(newPaymentMethod);

    console.log({ resValidation });

    if (resValidation.state) {
      newPaymentMethod.addPaymentMethod();
      return res.json({
        msg: 'Successful created payment Method',
        newPaymentMethod,
      });
    } else {
      return res.status(500).json(resValidation);
    }
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

const editPaymentMethods = (req, res) => {
  try {
    const { name } = req.body;
    const { id } = req.params;
    // console.log( { id } );
    const paymentMethod = new PaymentMethod(name, id);
    // console.log( { paymentMethod } );

    const indexPaymentMethod = paymentMethod
      .findAll()
      .findIndex((p) => p.id === id);
    // console.log( { indexPaymentMethod } );

    if (indexPaymentMethod < 0)
      return res
        .status(404)
        .json({
          status: false,
          msg: 'there is no payment method with that id',
        });

    console.log(req.body);

    paymentMethod.updateById(indexPaymentMethod);

    res.json({
      status: true,
      msg: 'payment method successfully updated',
    });
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

const deletePaymentMethods = (req, res) => {
  try {
    const { id } = req.params;
    // console.log( { id } );

    const paymentMethod = new PaymentMethod();
    // console.log( { paymentMethod } );

    const indexPM = paymentMethod.findAll().findIndex((p) => p.id === id);
    // console.log( { indexPM } );

    if (indexPM < 0)
      return res
        .status(404)
        .json({ status: false, msg: 'there is no product with that idc' });

    paymentMethod.deleteById(indexPM);

    res.json({
      status: true,
      msg: 'payment method successfully deleted',
    });
  } catch (error) {
    console.log({ error });
    return res.status(500).send(error);
  }
};

module.exports = {
  addPaymentMethod,
  getPaymentMethod,
  getPaymentMethods,
  addPaymentMethod,
  deletePaymentMethods,
  editPaymentMethods,
};
