const User = require( '../models/user.model' );
const { validationRegistro, validationLogin } = require( '../utils/validations' );
const myCustomAuthorizer = require( '../middlewares/basic-auth.middleware' );
const usersArray = require( '../data/user.data' );

const registerUser = ( req, res ) => {

  try {
    const { email, name, lastname, password, username } = req.body;

    const newUser = new User( username, email, name, lastname, password );

    // validacion de datos
    const resValidation = validationRegistro( newUser );

    console.log( { resValidation } );

    // agrega al usuario al array;
    if ( resValidation.state ) {
      newUser.addUser();

      console.log( { usuarios: newUser.findAll() } );

      return res.json({
				status: true,
				msg: 'User Registered successfully',
			});

    } else {
      console.log( 'fallo la validacion' );
      return res.status( 500 ).json( resValidation );
    }

  } catch ( error ) {
    console.log( error );
    return res.status( 500 ).send( error );
  }

};

const login = ( req, res ) => {

  try {
    const { username, password } = req.body;

    const resValidation = validationLogin( username, password );

    console.log( { resValidation } );

    if ( resValidation.state ) {
      const resp = myCustomAuthorizer( username, password );
      console.log( { resp } );

      return res.json( {
        ...resValidation.state,
        msg: 'successful login'
      } );
    } else {
      console.log( 'no se cumplio' );
      return res.status( 400 ).json( resValidation );
    }

  } catch ( error ) {
    console.log( error );
    return res.status( 500 ).send( error );
  }

};

module.exports = {
  registerUser,
  login
};