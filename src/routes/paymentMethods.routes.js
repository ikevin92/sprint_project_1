const { Router } = require('express');
const basicAuth = require('express-basic-auth');
const router = Router();

//controllers
const {
  getPaymentMethods,
  getPaymentMethod,
  addPaymentMethod,
  editPaymentMethods,
  deletePaymentMethods,
} = require('../controllers/paymentMethod.controller');

//middlewares
const { isAdmin } = require('../middlewares/user.middlewares');
const myCustomAuthorizer = require('../middlewares/basic-auth.middleware');

//routes
router.get(
  '/',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  getPaymentMethods,
);
router.get(
  '/:id',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  getPaymentMethod,
);
router.post(
  '/',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  addPaymentMethod,
);
router.put(
  '/:id',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  editPaymentMethods,
);
router.delete(
  '/:id',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  deletePaymentMethods,
);

module.exports = router;
