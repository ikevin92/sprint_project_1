const { Router } = require('express');
const basicAuth = require('express-basic-auth');
const router = Router();

// controllers
const {
  getOrders,
  getOrder,
  addOrder,
  editOrder,
  deleteOrder,
  changeStatusOrder,
} = require('../controllers/order.controller');

//middlewares
const { isAdmin } = require('../middlewares/user.middlewares');
const myCustomAuthorizer = require('../middlewares/basic-auth.middleware');
const { isClose } = require('../middlewares/order.middlewares');

// routes
router.get('/', [basicAuth({ authorizer: myCustomAuthorizer })], getOrders);
router.get('/:id', [basicAuth({ authorizer: myCustomAuthorizer })], getOrder);
router.post('/', [basicAuth({ authorizer: myCustomAuthorizer })], addOrder);
router.put(
  '/:id',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin, isClose],
  editOrder,
);
router.put(
  // Los administradores puedan ver todos los pedidos y cambiar el estado de los mismos.
  '/change-status/:id',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  changeStatusOrder,
);
router.delete(
  '/:id',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  deleteOrder,
);

module.exports = router;
