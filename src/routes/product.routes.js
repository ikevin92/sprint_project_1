const { Router } = require('express');
const basicAuth = require('express-basic-auth');
const router = Router();

//controllers
const {
  addProduct,
  getProducts,
  getProduct,
  editProduct,
  deleteProduct,
} = require('../controllers/product.controller');

//middlewares
const { isAdmin } = require('../middlewares/user.middlewares');
const myCustomAuthorizer = require('../middlewares/basic-auth.middleware');

// rutas
router.get('/', [basicAuth({ authorizer: myCustomAuthorizer })], getProducts);
router.get('/:id', [basicAuth({ authorizer: myCustomAuthorizer })], getProduct);
router.post(
  '/',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  addProduct,
);
router.put(
  '/:id',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  editProduct,
);
router.delete(
  '/:id',
  [basicAuth({ authorizer: myCustomAuthorizer }), isAdmin],
  deleteProduct,
);

module.exports = router;
