const { Router } = require('express');
const router = Router();

//controllers
const { registerUser, login } = require('../controllers/user.controller');

//middlewares

// rutas
router.post('/register', registerUser);
router.post('/login', login);

module.exports = router;
