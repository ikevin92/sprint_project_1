const User = require('../models/user.model');
const { decodeUser } = require('../utils/validations');

const isAdmin = (req, res, next) => {
  console.log('is Admin');
  console.log(req.headers.authorization);
  const user = new User();

  const { authorization } = req.headers;
  console.log({ authorization });

  const { username } = decodeUser(authorization);
  console.log({ username });

  const userFinded = user.findAll().filter((u) => u.username === username);

  if (userFinded[0].isAdmin) return next();

  return res.status(403).json({ state: false, msg: 'Required Admin role' });
};

module.exports = {
  isAdmin,
};
