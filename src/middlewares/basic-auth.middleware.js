const basicAuth = require('express-basic-auth');
const User = require('../models/user.model');

function myCustomAuthorizer(username, password) {
  console.log('entro al header');

  // console.log( { username, password });
  const user = new User();

  const users = user.findAll().filter((u) => u.username === username);
  // console.log(users)

  if (users.length <= 0) return false;

  const userMatches = basicAuth.safeCompare(username, users[0].username);
  const passwordMatches = basicAuth.safeCompare(password, users[0].password);

  return userMatches & passwordMatches;
}

module.exports = myCustomAuthorizer;
