const Order = require('../models/order.model');

const isClose = (req, res, next) => {
  console.log('isclose');
  const { id } = req.params;

  // se busca en el array
  const order = new Order();
  const orderFind = order.findOne(id);
  console.log({ orderFind });

  if (orderFind.length < 1)
    return res.status(500).json({
      state: false,
      msg: 'the order does not exist',
    });

  const { status } = orderFind[0];

  if (status.toLowerCase() !== 'confirmado') return next();

  return res.status(403).json({
    state: false,
    msg: 'the order is closed',
  });
};

module.exports = {
  isClose,
};
